﻿/**
 * LookupTable.cs
 * Created by: #Jesper_Vang# 
 * Created on: #24/09/2018#
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LookupTable : MonoBehaviour
{
    #region Variables

    public int x, y, z;
    public bool w;
    public List<objectVariables> table;

    #endregion

    #region Unity Methods

    public void Start()
    {
        createTable();
        testTable();
    }

    #endregion

    #region Methods

    public struct objectVariables
    {
        public int movespeed, tiltAngle, tiltLift;
        public bool pinnable;

        public objectVariables(int v1, int v2, int v3, bool v4)
        {
            movespeed = v1;
            tiltAngle = v2;
            tiltLift = v3;
            pinnable = v4;
        }
    }

    void createTable()
    {
        table = new List<objectVariables>();

        objectVariables character = new objectVariables     (15, 10, 5, true);
        objectVariables chair = new objectVariables         (15, 20, 10, true);
        objectVariables oven = new objectVariables          (5, 5, 5, false);
        objectVariables lamp = new objectVariables          (0, 10, 5, false);

        table.Add(character);
        table.Add(chair);
        table.Add(oven);
        table.Add(lamp);
    }

    public void testTable()
    {
        for (int i = 0; i < table.Count; i++)
        {
            objectVariables temp = table[i];

            x = temp.movespeed;
            y = temp.tiltAngle;
            z = temp.tiltLift;
            w = temp.pinnable;
        }
    }

    #endregion
}
