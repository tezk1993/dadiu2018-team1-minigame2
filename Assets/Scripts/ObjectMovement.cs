﻿/**
 * ObjectMovement.cs
 * Created by: The Superior Christian(W) 
 * Created on: 24/09/18
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectMovement : Movement{
    #region Variables
    public float speed = 5;
    #endregion

    #region Unity Methods

    public new void Awake()
    {
        
    }
	
    public new void Start()
    {
        
    }

    public new void Update()
    {
        base.Update();
        if (dir.x < -0.1 || dir.x > 0.1)
        {
            Debug.Log("should be Moving");
            Move(speed);
        }
    }

    #endregion

    #region Methods

    #endregion
}
