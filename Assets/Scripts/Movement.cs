﻿/**
 * Movement.cs
 * Created by: The Superior Christian(W) 
 * Created on: 24/09/18
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Movement : MonoBehaviour{
    #region Variables
    public Vector3 dir = Vector3.zero;
    #endregion

    #region Unity Methods

    public void Awake()
    {

    }

    public void Start()
    {
        
    }

    public void Update()
    {
        dir.x = Input.acceleration.x; //get input from the phone
        //Debug.Log("Before normalize = " + dir.x);
        if (dir.sqrMagnitude > 1)
        {
            dir.Normalize();  // normalize
        }
        //dir *= Time.deltaTime;
        Debug.Log("Movement = " + dir.x);
    }

    #endregion

    #region Methods
    public float GetDirX()
    {
        return dir.x;
    }
    public void Move(float speed)
    {
        transform.Translate(dir * speed * Time.deltaTime, Space.World);
    }

    void VisualTilt()
    {

    }
        #endregion
}
