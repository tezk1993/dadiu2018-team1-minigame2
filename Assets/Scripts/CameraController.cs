﻿/**
 * CameraController.cs
 * Created by: #Kasper M. Rasmussen#
 * Created on: #CREATIONDATE# (dd/mm/yy)
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
    {
    #region Variables
    private bool moving;
    private float maxDist;
    private Vector3 goal;
    private float timer;
    private float movementDuration;
    public Animator Animator;
    #endregion

    #region Unity Methods

    public void Start()
    {
        
    }

    public void Awake()
    {
        
    }

    public void Update()
    {
        if (moving)
        {
            timer += Time.deltaTime;
            if (timer < movementDuration)
            {
                transform.position = Vector3.MoveTowards(transform.position, goal, maxDist*Time.deltaTime);
            }
            else
            {
                moving = false;
            }
        }
        if (Input.GetMouseButtonDown(0))
        {
            FadeToBlack();
        }
        if (Input.GetMouseButtonDown(1))
        {
            FadeToTransparent();
        }
    }

    #endregion

    #region Methods
    public void MoveCamera(float duration, Vector3 goalPoint)
    {
        moving = true;
        goal = goalPoint;
        timer = 0;
        maxDist = Vector3.Distance(transform.position, goal)/duration;
        movementDuration = duration;
    }

    public void FadeToBlack()
    {
        Animator.SetTrigger("fadeToBlackTrigger");
    }
    public void FadeToTransparent()
    {
        Animator.SetTrigger("fadeToTransparentTrigger");
    }

    #endregion
}
